# C&CZ coding standard

The coding standard ruleset for C&CZ applications.

This specification extends and expands [PSR-12](https://github.com/php-fig/fig-standards/blob/master/proposed/extended-coding-style-guide.md),
the extended coding style guide and requires adherence to [PSR-1](https://www.php-fig.org/psr/psr-1),
the basic coding standard. These are minimal specifications and don't address all factors, including things like:

- whitespace around operators
- alignment of array keys and operators
- alignment of object operations
- how to format multi-line conditionals
- what and what not to import, and how
- etc.

Contributors have different coding styles and so do the maintainers. During code reviews there are regularly
discussions about spaces and alignments, where and when was said that a function needs to be imported. And
that's where this coding standard comes in: To have internal consistency in a component and between components.

## Installation

1. Install the module via composer by running:

    ```bash
    composer config repositories.gitlab-cncz-packages composer https://gitlab.science.ru.nl/api/v4/group/11159/-/packages/composer/packages.json
    composer require --dev cncz/php-coding-standard
    ```

2. Create file `phpcs.xml` on base path of your repository with this content:

   ```xml
   <?xml version="1.0"?>
   <ruleset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:noNamespaceSchemaLocation="vendor/squizlabs/php_codesniffer/phpcs.xsd">

       <arg name="basepath" value="."/>
       <arg name="cache" value=".phpcs-cache"/>
       <arg name="colors"/>
       <arg name="extensions" value="php"/>
       <arg name="parallel" value="80"/>

       <!-- Show progress -->
       <arg value="p"/>

       <!-- Paths to check -->
       <file>app</file>
       <file>config</file>
       <file>public</file>
       <file>test</file>

       <!-- Include all rules from the Laminas Coding Standard -->
       <rule ref="LaminasCodingStandard"/>
   </ruleset>
   ```

You can add or exclude some locations in that file.
For a reference please see: https://github.com/squizlabs/PHP_CodeSniffer/wiki/Annotated-Ruleset

## Usage

- To run checks only:

  ```bash
  composer cs-check
  ```

- To automatically fix many CS issues:

  ```bash
  composer cs-fix
  ```

## Ignoring parts of a File

> Note: Before PHP_CodeSniffer version 3.2.0, `// @codingStandardsIgnoreStart` and `// @codingStandardsIgnoreEnd` were
> used. These are deprecated and will be removed in PHP_CodeSniffer version 4.0.

Disable parts of a file:

```php
$xmlPackage = new XMLPackage;
// phpcs:disable
$xmlPackage['error_code'] = get_default_error_code_value();
$xmlPackage->send();
// phpcs:enable
```

Disable a specific rule:

```php
// phpcs:disable Generic.Commenting.Todo.Found
$xmlPackage = new XMLPackage;
$xmlPackage['error_code'] = get_default_error_code_value();
// TODO: Add an error message here.
$xmlPackage->send();
// phpcs:enable
```

Ignore a specific violation:

```php
$xmlPackage = new XMLPackage;
$xmlPackage['error_code'] = get_default_error_code_value();
// phpcs:ignore Generic.Commenting.Todo.Found
// TODO: Add an error message here.
$xmlPackage->send();
```
